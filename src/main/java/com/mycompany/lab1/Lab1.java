/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author user
 */

import java.util.Scanner;

public class Lab1 {
    

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcome to OX game!");
        
        char[][] GameBoard = {{' ', ' ', ' ',}, 
                            {' ', ' ', ' ',}, 
                            {' ', ' ', ' ',}};
        
        System.out.println(1 + "|" + 2 + "|" + 3);
        System.out.println("-+-+-");
        System.out.println(4 + "|" + 5 + "|" + 6);
        System.out.println("-+-+-");
        System.out.println(7 + "|" + 8 + "|" + 9);
        
        while(true){
            Oturn(kb, GameBoard);
            if(checkWhowin(GameBoard)==true){
                break; 
            }
            Xturn(kb, GameBoard);
            if(checkWhowin(GameBoard)==true){
                break; 
            } 
        }
        
    }

    

    public static boolean checkWhowin(char[][] GameBoard){
        if (checkWin(GameBoard, 'X')) {
            System.out.println("Congratulation !!!\nPlayer X win !!!");
            return true;
        }else if (checkWin(GameBoard, 'O')) {
            System.out.println("Congratulation !!!\nPlayer O win !!!");
            return true;
        }
        for(int i = 0; i < GameBoard.length; i++){
            for(int j = 0; j < GameBoard[i].length; j++){
                if(GameBoard[i][j] == ' '){
                    return false;
                }
            }
        }
        System.out.println("The game ended in the draw!!!");
        return true;
    }
    
    
    public static boolean checkWin(char[][] GameBoard , char symbol) {
            //vertical win
        if((GameBoard[0][0] == symbol && GameBoard[1][0] == symbol && GameBoard[2][0] == symbol)||
           (GameBoard[0][1] == symbol && GameBoard[1][1] == symbol && GameBoard[2][1] == symbol)||
           (GameBoard[0][2] == symbol && GameBoard[1][2] == symbol && GameBoard[2][2] == symbol)||
                
            //horizontal win
           (GameBoard[0][0] == symbol && GameBoard[0][1] == symbol && GameBoard[0][2] == symbol)||
           (GameBoard[1][0] == symbol && GameBoard[1][1] == symbol && GameBoard[1][2] == symbol)||
           (GameBoard[2][0] == symbol && GameBoard[2][1] == symbol && GameBoard[2][2] == symbol)||    
                
           //cross win
           (GameBoard[0][0] == symbol && GameBoard[1][1] == symbol && GameBoard[2][2] == symbol)||
           (GameBoard[0][2] == symbol && GameBoard[1][1] == symbol && GameBoard[2][0] == symbol)){
            return true;
        }
        return false;
        
        
    }

    public static void Oturn(Scanner kb, char[][] GameBoard) {
        System.out.println("Turn O");
        System.out.print("Please input position(1-9) : ");
        String playerO = kb.next();
        
        switch(playerO){
            case "1":
                GameBoard[0][0] = 'O';
                break;
            case "2":
                GameBoard[0][1] = 'O';
                break;
            case "3":
                GameBoard[0][2] = 'O';
                break;
            case "4":
                GameBoard[1][0] = 'O';
                break;
            case "5":
                GameBoard[1][1] = 'O';
                break;
            case "6":
                GameBoard[1][2] = 'O';
                break;
            case "7":
                GameBoard[2][0] = 'O';
                break;
            case "8":
                GameBoard[2][1] = 'O';
                break;
            case "9":
                GameBoard[2][2] = 'O';
                break;
            default:
                System.out.println("!!Please Input only(1-9)!!");
        }
        printGameBoard(GameBoard);
    }

    public static void Xturn(Scanner kb, char[][] GameBoard) {
        System.out.println("Turn X");
        System.out.print("Please input position(1-9) : ");
        String playerX = kb.next();
        
        switch(playerX){
            case "1":
                GameBoard[0][0] = 'X';
                break;
            case "2":
                GameBoard[0][1] = 'X';
                break;
            case "3":
                GameBoard[0][2] = 'X';
                break;
            case "4":
                GameBoard[1][0] = 'X';
                break;
            case "5":
                GameBoard[1][1] = 'X';
                break;
            case "6":
                GameBoard[1][2] = 'X';
                break;
            case "7":
                GameBoard[2][0] = 'X';
                break;
            case "8":
                GameBoard[2][1] = 'X';
                break;
            case "9":
                GameBoard[2][2] = 'X';
                break;
            default:
                System.out.println("!!Please Input only(1-9)!!");
        }
        printGameBoard(GameBoard);
    }
        
       
    public static void printGameBoard(char[][] GameBoard) {
        System.out.println(GameBoard[0][0] + "|" + GameBoard[0][1] + "|" + GameBoard[0][2]);
        System.out.println("-+-+-");
        System.out.println(GameBoard[1][0] + "|" + GameBoard[1][1] + "|" + GameBoard[1][2]);
        System.out.println("-+-+-");
        System.out.println(GameBoard[2][0] + "|" + GameBoard[2][1] + "|" + GameBoard[2][2]);
    }
    
    
}
